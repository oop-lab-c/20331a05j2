//Program to demonstrate Encpasulation 

#include<iostream>
using namespace std;
 
class AccessSpecifierDemo{
    private:
        int priVar;
    protected:
        int proVar;
    public:
        int pubVar;
    void setVar(int priValue,int proValue,int pubValue){
        priVar=priValue;
        proVar=proValue;
        pubVar=pubValue;
    }
    void getVar(){
        cout<<"Private value "<<priVar<<endl;
        cout<<"Protected value "<<proVar<<endl;
        cout<<"Public value "<<pubVar<<endl;
    }
};
int main(){
    cout<<"Enter the values of private,protected,public variables"<<endl;
    int pvt,pub,pro;
    cin>>pvt>>pro>>pub;         
    AccessSpecifierDemo obj;        
    obj.setVar(pvt,pro,pub);     
    obj.getVar();                   
}
