//Program to demonstrate template function and template class
#include <iostream>
using namespace std;

template <class T>
class Number {          
    T num;
   public:
    Number(T n){
       num = n;
    }  
    T getNum() {
        return num;
    }
};

template <typename S>           
S add(S a, S b){
    return a+b;
}
int main() {

    Number<char> Obj(97);
    Number<int> Obj2(97);
    cout << "char Number = " << Obj.getNum() << endl;       
    cout << "int Number = " << Obj2.getNum() << endl;       
    cout << "float addition = " << add < float > (1.6 , 8.2) << endl;           
    cout << "int addition = " << add < int > (6 , 8) << endl;                   
    return 0;
}