//Program to demonstrate partial abstraction
#include<iostream>
using namespace std;

class parent{
    public:
    void schoolchoice(){                         
        cout<<"Sarada public school"<<endl;
    }
    virtual void collegechoice()=0;               
};
class son : public parent{
    public:
    void collegechoice(){
        cout<<"Raghu Institute of technology"<<endl;
    }
};
class daughter : public parent{
    public:
    void collegechoice(){
        cout<<"MVGR College of Engineering"<<endl;
    } 
};
int main(){
    son s;
    daughter d;
    cout<<"Son's Name of School and College"<<endl;
    s.schoolchoice();               
    s.collegechoice();             
    cout<<"Daughter's Name of School and College"<<endl;
    d.schoolchoice();               
    d.collegechoice();              
}