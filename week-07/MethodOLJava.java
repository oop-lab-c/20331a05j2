//Program to demonstrate overloading in java

class overload {
    void print(int num){            
        System.out.println("Number "+ num);
    }
    void print(String name){        
        System.out.println("Name "+ name);
    }
}

public class MethodOLJava {
    public static void main(String[] args) {
        overload obj = new overload();
        obj.print(26);              
        obj.print("Sresta");      
    }
}

