//Program to demonstrate  pure abstraction in java using interface

interface parent{              
    void career();
}

class son implements parent{        
    public void career(){
        System.out.println("Son choose Photography as career");
    }
}

class daughter implements parent{       
    public void career(){
        System.out.println("Daughter choose Navy as career");
    }
}

public class PureAbsJava {
    public static void main(String args[]){
        son s = new son();
        daughter d = new daughter();
        s.career();
        d.career();
    }
}


