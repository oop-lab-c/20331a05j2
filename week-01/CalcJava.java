//3 week1
import java.util.*;
public class CalcJava
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Give data for first data : ");
        int a = input.nextInt();
        System.out.println("Give data for second data : ");
        int b = input.nextInt();
        System.out.println("Enter character : ");
        char c = input.next().charAt(0);
        if(c=='+')
        {
            System.out.println(a+b);
        }
        else if(c=='-')
        {
            System.out.println(a-b);
        }
        else if(c=='*')
        {
            System.out.println(a*b);
        }
        else if(c=='/')
        {
            System.out.println(a/b);
        }
        else
        {
            System.out.println(a%b);
        }

    }
}
