//Program to create thread by extending Thread class

class MyThread extends Thread{                  
    int variable=5;
    
    public void run(){                         
         for(int i=0;i<variable;i++){
             System.out.println(i);
         }    
    }
}
class ThreadClassJava{
    public static void main(String[] args){
        MyThread obj = new MyThread();
        obj.start();                              
    }
}

