//Program to demonstrate Simple Inheritance in java

import java.util.*;
class base{                                               
    void common(){
    System.out.println("English,Sanskrit,Physics,Chemisstry");
    }
} 
class MPC extends base{            
    void subjects(){
        System.out.println("Maths A,Maths B");
    }
}
class BIPC extends base{            
    void subjects(){
        System.out.println("Botany,Biology");
    }
}
 class SimplInheriJava {
    public static void main(String[] args) {
        MPC a=new MPC();
        BIPC b=new BIPC();
        System.out.println("MPC Subjects");
        a.common();             
        a.subjects();          
        System.out.println("BIPC Subjects");
        b.common();
        b.subjects();
    }
}

