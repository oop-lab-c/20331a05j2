//Program to demonstrate types of Inheritance

#include<iostream>
using namespace std;

class grandparent{ 
    public:
    grandparent(){cout<<"Types of inheritance "<<endl;}
};
class stranger{};   
class parent : public grandparent {         
    public:
    parent(){cout<<"Single inheritance"<<endl;}
};
class brother :  virtual public parent{     
    public:
    brother(){cout<<"Multilevel Inheritance "<<endl;}
};
class sister : virtual public parent{       
    public:
    sister(){cout<<"Hierarchial Inheritance "<<endl;}
};
class cousin : virtual public brother , virtual public sister{     
    public:
    cousin(){cout<<"Multiple Inheritance"<<endl;}
};
class grandchild : public cousin , public stranger{               
    public:
    grandchild(){cout<<"Hybrid Inheritance "<<endl;}
};
int main(){
    grandchild g;
    return 0;
}