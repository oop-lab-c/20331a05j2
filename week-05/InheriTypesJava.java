//Java only supports Simple, Multilevel and Hierarvhial Inheritance..

class grandparent{                              
    grandparent(){System.out.println("Types of Inheritance in Java ");}
}

class parent extends grandparent{               
    parent(){System.out.println("Single Inheritance ");}
}

class son extends parent{}                      

class daughter extends parent{                
    daughter(){
        System.out.println("Multilevel Inheritance ");
        System.out.println("Hierarchial Inheritance ");
    }  
}
public class InheriTypesJava {
    public static void main(String[] args){
        daughter obj = new daughter();
    }
}


